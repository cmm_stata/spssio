#include "stplugin.h"

#ifndef _MSC_VER
#define CALLBACK
#include <dlfcn.h>
#else
#include "windows.h"
#undef min
#undef max
#define CALLBACK __stdcall
#endif

#include "spssdiocodes.h"

#include <string>
#include <cstring>
#include <cstdio>
#include <vector>

typedef double(CALLBACK* SPSSSYSMISVAL)(void);
typedef int(CALLBACK* SPSSGETVARHANDLE)(const int, const char*, double*);

typedef int(CALLBACK* SPSSOPENWRITE)(const char*, int*);
typedef int(CALLBACK* SPSSSETVARNAME)(const int, const char*, const int);
typedef int(CALLBACK* SPSSSETVARLABEL)(const int, const char*, const char*);
typedef int(CALLBACK* SPSSSETVARNVALUELABEL)(const int, const char*, const double, const char*);
typedef int(CALLBACK* SPSSCOMMITHEADER)(const int);
typedef int(CALLBACK* SPSSSETVALUENUMERIC)(const int, const double, const double);
typedef int(CALLBACK* SPSSCOMMITCASERECORD)(const int);
typedef int(CALLBACK* SPSSCLOSEWRITE)(const int);
typedef int(CALLBACK* SPSSSETIDSTRING)(const int, const char*);

typedef int(CALLBACK* SPSSOPENREAD)(const char*, int*);
typedef int(CALLBACK* SPSSGETNUMBEROFVARIABLES)(const int, int*);
typedef int(CALLBACK* SPSSGETNUMBEROFCASES)(const int, long*);
typedef int(CALLBACK* SPSSGETVARNAMES)(const int, int*, char***, int**);
typedef int(CALLBACK* SPSSGETVARLABEL)(const int, const char*, char*);
typedef int(CALLBACK* SPSSGETVARNVALUELABELS)(const int, const char*, double**, char***, int*);
typedef int(CALLBACK* SPSSGETVARCVALUELABELS)(const int, const char*, char***, char***, int*);
typedef int(CALLBACK* SPSSFREEVARNVALUELABELS)(double*, char**, const int);
typedef int(CALLBACK* SPSSFREEVARCVALUELABELS)(char**, char**, const int);
typedef int(CALLBACK* SPSSREADCASERECORD)(const int);
typedef int(CALLBACK* SPSSGETVALUENUMERIC)(const int, const double, double*);
typedef int(CALLBACK* SPSSGETVARNMISSINGVALUES)(const int, const char*, int*, double*, double*, double*);
typedef int(CALLBACK* SPSSGETVALUECHAR)(const int, const double, char*, const int);
typedef int(CALLBACK* SPSSGETVARCMISSINGVALUES)(const int, const char*, int*, char*, char*, char*);
typedef int(CALLBACK* SPSSFREEVARNAMES)(char**, int*, const int);
typedef int(CALLBACK* SPSSCLOSEREAD)(const int);
typedef int(CALLBACK* SPSSGETIDSTRING)(const int, char*);
typedef int(CALLBACK* SPSSGETTIMESTAMP)(const int, char*, char*);
typedef int(CALLBACK* SPSSGETVARPRINTFORMAT)(const int, const char*, int*, int*, int*);
typedef int(CALLBACK* SPSSSETVARWRITEFORMAT)(const int, const char*, int, int, int);
typedef int(CALLBACK* SPSSSETVARPRINTFORMAT)(const int, const char*, int, int, int);
typedef int(CALLBACK* SPSSSETVALUECHAR)(const int, const double, const char*);

ST_retcode getdimensions(HINSTANCE HLib, const char* filename) {
	// Fetch number of records, number of variables

#ifdef _MSC_VER
	SPSSOPENREAD spssOpenRead = (SPSSOPENREAD)GetProcAddress(HLib, "spssOpenRead");
	SPSSGETNUMBEROFVARIABLES spssGetNumberofVariables = (SPSSGETNUMBEROFVARIABLES)GetProcAddress(HLib, "spssGetNumberofVariables");
	SPSSGETNUMBEROFCASES spssGetNumberofCases = (SPSSGETNUMBEROFCASES)GetProcAddress(HLib, "spssGetNumberofCases");
	SPSSCLOSEREAD spssCloseRead = (SPSSCLOSEREAD)GetProcAddress(HLib, "spssCloseRead");
#else
	SPSSOPENREAD spssOpenRead = (SPSSOPENREAD)dlsym(HLib, "spssOpenRead");
	SPSSGETNUMBEROFVARIABLES spssGetNumberofVariables = (SPSSGETNUMBEROFVARIABLES)dlsym(HLib, "spssGetNumberofVariables");
	SPSSGETNUMBEROFCASES spssGetNumberofCases = (SPSSGETNUMBEROFCASES)dlsym(HLib, "spssGetNumberofCases");
	SPSSCLOSEREAD spssCloseRead = (SPSSCLOSEREAD)dlsym(HLib, "spssCloseRead");
	char *err = dlerror();
	if (err != nullptr) {
		SF_error(err);
		return(198);
	}
#endif

	if (!spssOpenRead
		|| !spssGetNumberofVariables
		|| !spssGetNumberofCases
		|| !spssCloseRead) {
		SF_error("Missing function\n");
		return(198);
	}

	int handle;

	int errCode = spssOpenRead(filename, &handle);
	if (errCode != SPSS_OK) {
		SF_error("Error opening file\n");
		return(errCode);
	}

	int nvars = 0;
	errCode = spssGetNumberofVariables(handle, &nvars);
	if (errCode != SPSS_OK) {
		SF_error("Error reading number of observations\n");
		return(errCode);
	}

	ST_retcode rc;
	char buf[1000];

	if (rc = SF_macro_use("_nvars", buf, sizeof(buf))) return(rc);
	if (rc = SF_scal_save(buf, nvars)) return(rc);

	long nobs = 0;
	errCode = spssGetNumberofCases(handle, &nobs);
	if (errCode != SPSS_OK) {
		SF_error("Error reading number of cases\n");
		return(errCode);
	}
	if (rc = SF_macro_use("_nobs", buf, sizeof(buf))) return(rc);
	if (rc = SF_scal_save(buf, nobs)) return(rc);

	errCode = spssCloseRead(handle);
	if (errCode != SPSS_OK) {
		SF_error("Error closing file\n");
		return(errCode);
	}
	return(0);
}

ST_retcode getnumlabels(HINSTANCE HLib, const char* filename) {
	// Fetch number of records, number of variables

#ifdef _MSC_VER
	SPSSOPENREAD spssOpenRead = (SPSSOPENREAD)GetProcAddress(HLib, "spssOpenRead");
	SPSSGETNUMBEROFVARIABLES spssGetNumberofVariables = (SPSSGETNUMBEROFVARIABLES)GetProcAddress(HLib, "spssGetNumberofVariables");
	SPSSGETNUMBEROFCASES spssGetNumberofCases = (SPSSGETNUMBEROFCASES)GetProcAddress(HLib, "spssGetNumberofCases");
	SPSSGETVARNAMES spssGetVarNames = (SPSSGETVARNAMES)GetProcAddress(HLib, "spssGetVarNames");
	SPSSGETVARNVALUELABELS spssGetVarNValueLabels = (SPSSGETVARNVALUELABELS)GetProcAddress(HLib, "spssGetVarNValueLabels");
	SPSSGETVARCVALUELABELS spssGetVarCValueLabels = (SPSSGETVARCVALUELABELS)GetProcAddress(HLib, "spssGetVarCValueLabels");
	SPSSFREEVARNVALUELABELS spssFreeVarNValueLabels = (SPSSFREEVARNVALUELABELS)GetProcAddress(HLib, "spssFreeVarNValueLabels");
	SPSSFREEVARCVALUELABELS spssFreeVarCValueLabels = (SPSSFREEVARCVALUELABELS)GetProcAddress(HLib, "spssFreeVarCValueLabels");
	SPSSCLOSEREAD spssCloseRead = (SPSSCLOSEREAD)GetProcAddress(HLib, "spssCloseRead");
#else
	SPSSOPENREAD spssOpenRead = (SPSSOPENREAD)dlsym(HLib, "spssOpenRead");
	SPSSGETNUMBEROFVARIABLES spssGetNumberofVariables = (SPSSGETNUMBEROFVARIABLES)dlsym(HLib, "spssGetNumberofVariables");
	SPSSGETNUMBEROFCASES spssGetNumberofCases = (SPSSGETNUMBEROFCASES)dlsym(HLib, "spssGetNumberofCases");
	SPSSGETVARNAMES spssGetVarNames = (SPSSGETVARNAMES)dlsym(HLib, "spssGetVarNames");
	SPSSGETVARNVALUELABELS spssGetVarNValueLabels = (SPSSGETVARNVALUELABELS)dlsym(HLib, "spssGetVarNValueLabels");
	SPSSGETVARCVALUELABELS spssGetVarCValueLabels = (SPSSGETVARCVALUELABELS)dlsym(HLib, "spssGetVarCValueLabels");
	SPSSFREEVARNVALUELABELS spssFreeVarNValueLabels = (SPSSFREEVARNVALUELABELS)dlsym(HLib, "spssFreeVarNValueLabels");
	SPSSFREEVARCVALUELABELS spssFreeVarCValueLabels = (SPSSFREEVARCVALUELABELS)dlsym(HLib, "spssFreeVarCValueLabels");
	SPSSCLOSEREAD spssCloseRead = (SPSSCLOSEREAD)dlsym(HLib, "spssCloseRead");
	char *err = dlerror();
	if (err != nullptr) {
		SF_error(err);
		return(198);
	}
#endif

	if (!spssOpenRead
		|| !spssGetNumberofVariables
		|| !spssGetNumberofCases
		|| !spssGetVarNames
		|| !spssGetVarNValueLabels
		|| !spssGetVarCValueLabels
		|| !spssFreeVarNValueLabels
		|| !spssFreeVarCValueLabels
		|| !spssCloseRead) {
		SF_error("Missing function\n");
		return(198);
	}

	int handle;

	int errCode = spssOpenRead(filename, &handle);
	if (errCode != SPSS_OK) {
		SF_error("Error opening file\n");
		return(errCode);
	}

	int nvars = 0;
	errCode = spssGetNumberofVariables(handle, &nvars);
	if (errCode != SPSS_OK) {
		SF_error("Error reading number of observations\n");
		return(errCode);
	}

	ST_retcode rc;
	char buf[1000];

	if (rc = SF_macro_use("_nvars", buf, sizeof(buf))) return(rc);
	double nvars_expect = 0;
	if (rc = SF_scal_use(buf, &nvars_expect)) return(rc);

	if (nvars != nvars_expect) {
		SF_error("Unexpected number of variables\n");
		return(198);
	}

	long nobs = 0;
	errCode = spssGetNumberofCases(handle, &nobs);
	if (errCode != SPSS_OK) {
		SF_error("Error reading number of cases\n");
		return(errCode);
	}
	if (rc = SF_macro_use("_nobs", buf, sizeof(buf))) return(rc);
	double nobs_expect = 0;
	if (rc = SF_scal_use(buf, &nobs_expect)) return(rc);

	if (nobs != nobs_expect) {
		SF_error("Unexpected number of observations\n");
		return(198);
	}

	int *vartypes;
	char **varnames;
	errCode = spssGetVarNames(handle, &nvars, &varnames, &vartypes);
	if (errCode != SPSS_OK) {
		SF_error("Error reading variable names\n");
		return(errCode);
	}

	for (int k = 0; k < nvars; k++) {
		char macname[100];
		int numL;
		char **labelsL;
		if (vartypes[k] == 0) {
			double *nValuesL;
			errCode = spssGetVarNValueLabels(handle, varnames[k], &nValuesL, &labelsL, &numL);
			if (errCode != SPSS_OK && errCode != SPSS_NO_LABELS) {
				SF_error("Error reading value labels\n");
				return(errCode);
			}
			sprintf(macname, "_var%dnlab", k);
			if (rc = SF_macro_use(macname, buf, sizeof(buf))) return(rc);
			if (rc = SF_scal_save(buf, numL)) return(rc);
			errCode = spssFreeVarNValueLabels(nValuesL, labelsL, numL);
			if (errCode != SPSS_OK) {
				SF_error("Error freeing value labels\n");
				return(errCode);
			}
		} else {
			char **cValuesL;
			errCode = spssGetVarCValueLabels(handle, varnames[k], &cValuesL, &labelsL, &numL);
			if (errCode != SPSS_OK && errCode != SPSS_NO_LABELS) {
				SF_error("Error reading value labels\n");
				return(errCode);
			}
			sprintf(macname, "_var%dnlab", k);
			if (rc = SF_macro_use(macname, buf, sizeof(buf))) return(rc);
			if (rc = SF_scal_save(buf, numL)) return(rc);
			errCode = spssFreeVarCValueLabels(cValuesL, labelsL, numL);
			if (errCode != SPSS_OK) {
				SF_error("Error freeing value labels\n");
				return(errCode);
			}
		}
	}

	errCode = spssCloseRead(handle);
	if (errCode != SPSS_OK) {
		SF_error("Error closing file\n");
		return(errCode);
	}
	return(0);
}

ST_retcode query(HINSTANCE HLib, const char* filename) {
	//SF_display("Querying File\n");
	// Fetch number of records, number of variables, etc

#ifdef _MSC_VER
	SPSSOPENREAD spssOpenRead = (SPSSOPENREAD)GetProcAddress(HLib, "spssOpenRead");
	SPSSGETNUMBEROFVARIABLES spssGetNumberofVariables = (SPSSGETNUMBEROFVARIABLES)GetProcAddress(HLib, "spssGetNumberofVariables");
	SPSSGETNUMBEROFCASES spssGetNumberofCases = (SPSSGETNUMBEROFCASES)GetProcAddress(HLib, "spssGetNumberofCases");
	SPSSGETVARNAMES spssGetVarNames = (SPSSGETVARNAMES)GetProcAddress(HLib, "spssGetVarNames");
	SPSSGETVARLABEL spssGetVarLabel = (SPSSGETVARLABEL)GetProcAddress(HLib, "spssGetVarLabel");
	SPSSGETVARNVALUELABELS spssGetVarNValueLabels = (SPSSGETVARNVALUELABELS)GetProcAddress(HLib, "spssGetVarNValueLabels");
	SPSSGETVARCVALUELABELS spssGetVarCValueLabels = (SPSSGETVARCVALUELABELS)GetProcAddress(HLib, "spssGetVarCValueLabels");
	SPSSFREEVARNVALUELABELS spssFreeVarNValueLabels = (SPSSFREEVARNVALUELABELS)GetProcAddress(HLib, "spssFreeVarNValueLabels");
	SPSSFREEVARCVALUELABELS spssFreeVarCValueLabels = (SPSSFREEVARCVALUELABELS)GetProcAddress(HLib, "spssFreeVarCValueLabels");
	SPSSFREEVARNAMES spssFreeVarNames = (SPSSFREEVARNAMES)GetProcAddress(HLib, "spssFreeVarNames");
	SPSSCLOSEREAD spssCloseRead = (SPSSCLOSEREAD)GetProcAddress(HLib, "spssCloseRead");
	SPSSGETIDSTRING spssGetIdString = (SPSSGETIDSTRING)GetProcAddress(HLib, "spssGetIdString");
	SPSSGETTIMESTAMP spssGetTimeStamp = (SPSSGETTIMESTAMP)GetProcAddress(HLib, "spssGetTimeStamp");
	SPSSGETVARPRINTFORMAT spssGetVarPrintFormat = (SPSSGETVARPRINTFORMAT)GetProcAddress(HLib, "spssGetVarPrintFormat");
#else
	SPSSOPENREAD spssOpenRead = (SPSSOPENREAD)dlsym(HLib, "spssOpenRead");
	SPSSGETNUMBEROFVARIABLES spssGetNumberofVariables = (SPSSGETNUMBEROFVARIABLES)dlsym(HLib, "spssGetNumberofVariables");
	SPSSGETNUMBEROFCASES spssGetNumberofCases = (SPSSGETNUMBEROFCASES)dlsym(HLib, "spssGetNumberofCases");
	SPSSGETVARNAMES spssGetVarNames = (SPSSGETVARNAMES)dlsym(HLib, "spssGetVarNames");
	SPSSGETVARLABEL spssGetVarLabel = (SPSSGETVARLABEL)dlsym(HLib, "spssGetVarLabel");
	SPSSGETVARNVALUELABELS spssGetVarNValueLabels = (SPSSGETVARNVALUELABELS)dlsym(HLib, "spssGetVarNValueLabels");
	SPSSGETVARCVALUELABELS spssGetVarCValueLabels = (SPSSGETVARCVALUELABELS)dlsym(HLib, "spssGetVarCValueLabels");
	SPSSFREEVARNVALUELABELS spssFreeVarNValueLabels = (SPSSFREEVARNVALUELABELS)dlsym(HLib, "spssFreeVarNValueLabels");
	SPSSFREEVARCVALUELABELS spssFreeVarCValueLabels = (SPSSFREEVARCVALUELABELS)dlsym(HLib, "spssFreeVarCValueLabels");
	SPSSFREEVARNAMES spssFreeVarNames = (SPSSFREEVARNAMES)dlsym(HLib, "spssFreeVarNames");
	SPSSCLOSEREAD spssCloseRead = (SPSSCLOSEREAD)dlsym(HLib, "spssCloseRead");
	SPSSGETIDSTRING spssGetIdString = (SPSSGETIDSTRING)dlsym(HLib, "spssGetIdString");
	SPSSGETTIMESTAMP spssGetTimeStamp = (SPSSGETTIMESTAMP)dlsym(HLib, "spssGetTimeStamp");
	SPSSGETVARPRINTFORMAT spssGetVarPrintFormat = (SPSSGETVARPRINTFORMAT)dlsym(HLib, "spssGetVarPrintFormat");
	char *err = dlerror();
	if (err != nullptr) {
		SF_error(err);
		return(198);
	}
#endif

	if (!spssOpenRead
		|| !spssGetNumberofVariables
		|| !spssGetNumberofCases
		|| !spssGetVarNames
		|| !spssGetVarLabel
		|| !spssGetVarNValueLabels
		|| !spssGetVarCValueLabels
		|| !spssFreeVarNValueLabels
		|| !spssFreeVarCValueLabels
		|| !spssFreeVarNames
		|| !spssCloseRead
		|| !spssGetIdString
		|| !spssGetTimeStamp
		|| !spssGetVarPrintFormat) {
		SF_error("Missing function\n");
		return(198);
	}

	int handle;

	int errCode = spssOpenRead(filename, &handle);
	if (errCode != SPSS_OK) {
		SF_error("Error opening file\n");
		return(errCode);
	}

	int nvars = 0;
	errCode = spssGetNumberofVariables(handle, &nvars);
	if (errCode != SPSS_OK) {
		SF_error("Error reading number of observations\n");
		return(errCode);
	}

	ST_retcode rc;
	char buf[1000];

	if (rc = SF_macro_use("_nvars", buf, sizeof(buf))) return(rc);
	double nvars_expect = 0;
	if (rc = SF_scal_use(buf, &nvars_expect)) return(rc);

	if (nvars != nvars_expect) {
		SF_error("Unexpected number of variables\n");
		return(198);
	}

	long nobs = 0;
	errCode = spssGetNumberofCases(handle, &nobs);
	if (errCode != SPSS_OK) {
		SF_error("Error reading number of cases\n");
		return(errCode);
	}
	if (rc = SF_macro_use("_nobs", buf, sizeof(buf))) return(rc);
	double nobs_expect = 0;
	if (rc = SF_scal_use(buf, &nobs_expect)) return(rc);

	if (nobs != nobs_expect) {
		SF_error("Unexpected number of observations\n");
		return(198);
	}

	char id[65];
	errCode = spssGetIdString(handle, id);
	if (errCode != SPSS_OK) {
		SF_error("Error reading dataset name\n");
		return(errCode);
	}

	if (rc = SF_macro_save("_dataid", id)) return(rc);

	char date[10];
	char time[9];
	errCode = spssGetTimeStamp(handle, date, time);
	if (errCode != SPSS_OK) {
		SF_error("Error reading dataset timestamp\n");
		return(errCode);
	}

	if (rc = SF_macro_save("_date", date)) return(rc);
	if (rc = SF_macro_save("_time", time)) return(rc);

	int *vartypes;
	char **varnames;
	errCode = spssGetVarNames(handle, &nvars, &varnames, &vartypes);
	if (errCode != SPSS_OK) {
		SF_error("Error reading variable names\n");
		return(errCode);
	}

	for (int k = 0; k < nvars; k++) {
		char macname[100];
		sprintf(macname, "_var%dname", k);
		if (rc = SF_macro_save(macname, varnames[k])) return(rc);
		sprintf(macname, "_var%dtype", k);
		if (rc = SF_macro_use(macname, buf, sizeof(buf))) return(rc);
		if (rc = SF_scal_save(buf, vartypes[k])) return(rc);

		char vLabel[121];
		errCode = spssGetVarLabel(handle, varnames[k], vLabel);
		if (errCode != SPSS_OK) {
			if (errCode == SPSS_NO_LABEL) {
				sprintf(vLabel, "%s", "");
			} else {
				SF_error("Error reading variable label\n");
				return(errCode);
			}
		}
		sprintf(macname, "_var%dlabel", k);
		if (rc = SF_macro_save(macname, vLabel)) return(rc);

		int type;
		int dec;
		int wid;

		//errCode = spssGetVarWriteFormat(handle, varnames[k], &type, &dec, &wid);
		errCode = spssGetVarPrintFormat(handle, varnames[k], &type, &dec, &wid);
		if (errCode != SPSS_OK) {
			SF_error("Error reading variable format information\n");
			return(errCode);
		}
		sprintf(macname, "_var%ddisptype", k);
		if (rc = SF_macro_use(macname, buf, sizeof(buf))) return(rc);
		if (rc = SF_scal_save(buf, type)) return(rc);
		sprintf(macname, "_var%ddispdec", k);
		if (rc = SF_macro_use(macname, buf, sizeof(buf))) return(rc);
		if (rc = SF_scal_save(buf, dec)) return(rc);
		sprintf(macname, "_var%ddispwid", k);
		if (rc = SF_macro_use(macname, buf, sizeof(buf))) return(rc);
		if (rc = SF_scal_save(buf, wid)) return(rc);

		int numL;
		char **labelsL;
		if (vartypes[k] == 0) {
			double *nValuesL;
			errCode = spssGetVarNValueLabels(handle, varnames[k], &nValuesL, &labelsL, &numL);
			if (errCode != SPSS_OK && errCode != SPSS_NO_LABELS) {
				SF_error("Error reading value labels\n");
				return(errCode);
			}
			sprintf(macname, "_var%dnlab", k);
			if (rc = SF_macro_use(macname, buf, sizeof(buf))) return(rc);
			if (rc = SF_scal_save(buf, numL)) return(rc);
			for (int i = 0; i < numL; i++) {
				sprintf(macname, "_var%dlab%dnum", k, i);
				if (rc = SF_macro_use(macname, buf, sizeof(buf))) return(rc);
				if (rc = SF_scal_save(buf, nValuesL[i])) return(rc);
				sprintf(macname, "_var%dlab%dval", k, i);
				if (rc = SF_macro_save(macname, labelsL[i])) return(rc);
			}
			errCode = spssFreeVarNValueLabels(nValuesL, labelsL, numL);
			if (errCode != SPSS_OK) {
				SF_error("Error freeing value labels\n");
				return(errCode);
			}
		} else {
			char **cValuesL;
			errCode = spssGetVarCValueLabels(handle, varnames[k], &cValuesL, &labelsL, &numL);
			if (errCode != SPSS_OK && errCode != SPSS_NO_LABELS) {
				SF_error("Error reading value labels\n");
				return(errCode);
			}
			sprintf(macname, "_var%dnlab", k);
			if (rc = SF_macro_use(macname, buf, sizeof(buf))) return(rc);
			if (rc = SF_scal_save(buf, numL)) return(rc);
			for (int i = 0; i < numL; i++) {
				sprintf(macname, "_var%dlab%dnum", k, i);
				if (rc = SF_macro_save(macname, cValuesL[i])) return(rc);

				sprintf(macname, "_var%dlab%dval", k, i);
				if (rc = SF_macro_save(macname, labelsL[i])) return(rc);
			}
			errCode = spssFreeVarCValueLabels(cValuesL, labelsL, numL);
			if (errCode != SPSS_OK) {
				SF_error("Error freeing value labels\n");
				return(errCode);
			}
		}
	}
	errCode = spssFreeVarNames(varnames, vartypes, nvars);
	if (errCode != SPSS_OK) {
		SF_error("Error freeing variable names\n");
		return(errCode);
	}
	errCode = spssCloseRead(handle);
	if (errCode != SPSS_OK) {
		SF_error("Error closing file\n");
		return(errCode);
	}
	return(0);
}

ST_retcode load(HINSTANCE HLib, const char* filename) {
	// Load data

#ifdef _MSC_VER
	SPSSOPENREAD spssOpenRead = (SPSSOPENREAD)GetProcAddress(HLib, "spssOpenRead");
	SPSSGETNUMBEROFVARIABLES spssGetNumberofVariables = (SPSSGETNUMBEROFVARIABLES)GetProcAddress(HLib, "spssGetNumberofVariables");
	SPSSGETNUMBEROFCASES spssGetNumberofCases = (SPSSGETNUMBEROFCASES)GetProcAddress(HLib, "spssGetNumberofCases");
	SPSSGETVARNAMES spssGetVarNames = (SPSSGETVARNAMES)GetProcAddress(HLib, "spssGetVarNames");
	SPSSGETVARLABEL spssGetVarLabel = (SPSSGETVARLABEL)GetProcAddress(HLib, "spssGetVarLabel");
	SPSSGETVARNVALUELABELS spssGetVarNValueLabels = (SPSSGETVARNVALUELABELS)GetProcAddress(HLib, "spssGetVarNValueLabels");
	SPSSGETVARCVALUELABELS spssGetVarCValueLabels = (SPSSGETVARCVALUELABELS)GetProcAddress(HLib, "spssGetVarCValueLabels");
	SPSSFREEVARNVALUELABELS spssFreeVarNValueLabels = (SPSSFREEVARNVALUELABELS)GetProcAddress(HLib, "spssFreeVarNValueLabels");
	SPSSFREEVARCVALUELABELS spssFreeVarCValueLabels = (SPSSFREEVARCVALUELABELS)GetProcAddress(HLib, "spssFreeVarCValueLabels");
	SPSSREADCASERECORD spssReadCaseRecord = (SPSSREADCASERECORD)GetProcAddress(HLib, "spssReadCaseRecord");
	SPSSGETVARHANDLE spssGetVarHandle = (SPSSGETVARHANDLE)GetProcAddress(HLib, "spssGetVarHandle");
	SPSSGETVALUENUMERIC spssGetValueNumeric = (SPSSGETVALUENUMERIC)GetProcAddress(HLib, "spssGetValueNumeric");
	SPSSGETVARNMISSINGVALUES spssGetVarNMissingValues = (SPSSGETVARNMISSINGVALUES)GetProcAddress(HLib, "spssGetVarNMissingValues");
	SPSSGETVALUECHAR spssGetValueChar = (SPSSGETVALUECHAR)GetProcAddress(HLib, "spssGetValueChar");
	SPSSGETVARCMISSINGVALUES spssGetVarCMissingValues = (SPSSGETVARCMISSINGVALUES)GetProcAddress(HLib, "spssGetVarCMissingValues");
	SPSSFREEVARNAMES spssFreeVarNames = (SPSSFREEVARNAMES)GetProcAddress(HLib, "spssFreeVarNames");
	SPSSCLOSEREAD spssCloseRead = (SPSSCLOSEREAD)GetProcAddress(HLib, "spssCloseRead");
	SPSSSYSMISVAL spssSysmisVal = (SPSSSYSMISVAL)GetProcAddress(HLib, "spssSysmisVal");
	SPSSGETIDSTRING spssGetIdString = (SPSSGETIDSTRING)GetProcAddress(HLib, "spssGetIdString");
#else
	SPSSOPENREAD spssOpenRead = (SPSSOPENREAD)dlsym(HLib, "spssOpenRead");
	SPSSGETNUMBEROFVARIABLES spssGetNumberofVariables = (SPSSGETNUMBEROFVARIABLES)dlsym(HLib, "spssGetNumberofVariables");
	SPSSGETNUMBEROFCASES spssGetNumberofCases = (SPSSGETNUMBEROFCASES)dlsym(HLib, "spssGetNumberofCases");
	SPSSGETVARNAMES spssGetVarNames = (SPSSGETVARNAMES)dlsym(HLib, "spssGetVarNames");
	SPSSGETVARLABEL spssGetVarLabel = (SPSSGETVARLABEL)dlsym(HLib, "spssGetVarLabel");
	SPSSGETVARNVALUELABELS spssGetVarNValueLabels = (SPSSGETVARNVALUELABELS)dlsym(HLib, "spssGetVarNValueLabels");
	SPSSGETVARCVALUELABELS spssGetVarCValueLabels = (SPSSGETVARCVALUELABELS)dlsym(HLib, "spssGetVarCValueLabels");
	SPSSFREEVARNVALUELABELS spssFreeVarNValueLabels = (SPSSFREEVARNVALUELABELS)dlsym(HLib, "spssFreeVarNValueLabels");
	SPSSFREEVARCVALUELABELS spssFreeVarCValueLabels = (SPSSFREEVARCVALUELABELS)dlsym(HLib, "spssFreeVarCValueLabels");
	SPSSREADCASERECORD spssReadCaseRecord = (SPSSREADCASERECORD)dlsym(HLib, "spssReadCaseRecord");
	SPSSGETVARHANDLE spssGetVarHandle = (SPSSGETVARHANDLE)dlsym(HLib, "spssGetVarHandle");
	SPSSGETVALUENUMERIC spssGetValueNumeric = (SPSSGETVALUENUMERIC)dlsym(HLib, "spssGetValueNumeric");
	SPSSGETVARNMISSINGVALUES spssGetVarNMissingValues = (SPSSGETVARNMISSINGVALUES)dlsym(HLib, "spssGetVarNMissingValues");
	SPSSGETVALUECHAR spssGetValueChar = (SPSSGETVALUECHAR)dlsym(HLib, "spssGetValueChar");
	SPSSGETVARCMISSINGVALUES spssGetVarCMissingValues = (SPSSGETVARCMISSINGVALUES)dlsym(HLib, "spssGetVarCMissingValues");
	SPSSFREEVARNAMES spssFreeVarNames = (SPSSFREEVARNAMES)dlsym(HLib, "spssFreeVarNames");
	SPSSCLOSEREAD spssCloseRead = (SPSSCLOSEREAD)dlsym(HLib, "spssCloseRead");
	SPSSSYSMISVAL spssSysmisVal = (SPSSSYSMISVAL)dlsym(HLib, "spssSysmisVal");
	SPSSGETIDSTRING spssGetIdString = (SPSSGETIDSTRING)dlsym(HLib, "spssGetIdString");
	char *err = dlerror();
	if (err != nullptr) {
		SF_error(err);
		return(198);
	}
#endif

	if (!spssOpenRead
		|| !spssGetNumberofVariables
		|| !spssGetNumberofCases
		|| !spssGetVarNames
		|| !spssGetVarLabel
		|| !spssGetVarNValueLabels
		|| !spssGetVarCValueLabels
		|| !spssFreeVarNValueLabels
		|| !spssFreeVarCValueLabels
		|| !spssReadCaseRecord
		|| !spssGetVarHandle
		|| !spssGetValueNumeric
		|| !spssGetVarNMissingValues
		|| !spssGetValueChar
		|| !spssGetVarCMissingValues
		|| !spssFreeVarNames
		|| !spssCloseRead
		|| !spssSysmisVal
		|| !spssGetIdString) {
		SF_error("Missing function\n");
		return(198);
	}

	//SF_display("Loading data\n");

	ST_retcode rc;
	char buf[1000];

	if (rc = SF_macro_use("_instart", buf, sizeof(buf))) return(rc);
	double instart = 0;
	if (rc = SF_scal_use(buf, &instart)) return(rc);

	if (rc = SF_macro_use("_inend", buf, sizeof(buf))) return(rc);
	double inend = 0;
	if (rc = SF_scal_use(buf, &inend)) return(rc);

	int handle;

	int errCode = spssOpenRead(filename, &handle);
	if (errCode != SPSS_OK) {
		SF_error("Error opening file\n");
		return(errCode);
	}

	int nvars = 0;
	errCode = spssGetNumberofVariables(handle, &nvars);
	if (errCode != SPSS_OK) {
		SF_error("Error reading number of variables file\n");
		return(errCode);
	}

	//if (nvars != SF_nvars()) {
	//	SF_error("Number of variables does not match\n");
	//}

	//if (nvars > SF_nvars()) {
	//	SF_error("Not enough variables allocated to hold data\n");
	//	return(198);
	//}

	long nobs = 0;
	errCode = spssGetNumberofCases(handle, &nobs);
	if (errCode != SPSS_OK) {
		SF_error("Error reading number of cases\n");
		return(errCode);
	}

	if (/*nobs*/ inend - instart + 1 != SF_nobs()) {
		SF_error("Number of observations does not match\n");
	}

	if (inend - instart + 1 > SF_nobs()) {
		SF_error("Not enough observations to hold data\n");
		return(198);
	}

	int *vartypes;
	char **varnames;
	errCode = spssGetVarNames(handle, &nvars, &varnames, &vartypes);
	if (errCode != SPSS_OK) {
		SF_error("Error reading variable names\n");
		return(errCode);
	}

	std::vector<int> incvars(nvars);
	for (int k = 0; k < nvars; k++) {
		char macname[100];
		sprintf(macname, "_var%dinc", k);
		if (rc = SF_macro_use(macname, buf, sizeof(buf))) return(rc);
		double incvar = 0;
		if (rc = SF_scal_use(buf, &incvar)) return(rc);
		incvars[k] = int(incvar);
	}

	int stataobs = 0;
	for (int c = 1; c <= nobs; c++) {
		errCode = spssReadCaseRecord(handle);
		if (errCode != SPSS_OK) {
			SF_error("Error reading case record\n");
			return(errCode);
		}
		if (c < instart || c > inend) {
			continue;
		}
		stataobs++;
		int statavar = 0;
		for (int k = 0; k < nvars; k++) {
			if (incvars[k] == 0) {
				continue;
			}
			statavar++;

			double varHandle;
			errCode = spssGetVarHandle(handle, varnames[k], &varHandle);
			if (errCode != SPSS_OK) {
				SF_error("Error getting variable handle\n");
				return(errCode);
			}
			if (vartypes[k] == 0) { // numeric
				double val = 0.0;
				errCode = spssGetValueNumeric(handle, varHandle, &val);
				if (errCode != SPSS_OK) {
					SF_error("Error reading numeric variable\n");
					return(errCode);
				}
				if (val == spssSysmisVal()) {
					if (rc = SF_vstore(statavar, stataobs, SV_missval)) return(rc);
				} else {
					if (rc = SF_vstore(statavar, stataobs, val)) return(rc);
				}
			} else { //string
				std::vector<char> cval(vartypes[k] + 1);
				errCode = spssGetValueChar(handle, varHandle, &cval[0], vartypes[k] + 1);
				if (errCode != SPSS_OK) {
					SF_error("Error reading string variable\n");
					return(errCode);
				}
				if (rc = SF_sstore(statavar, stataobs, &cval[0])) return(rc);
			}
		}
	}

	errCode = spssCloseRead(handle);
	if (errCode != SPSS_OK) {
		SF_error("Error closing file\n");
		return(errCode);
	}
	return(0);
}

ST_retcode save(HINSTANCE HLib, const char* filename) {
	// Save data

#ifdef _MSC_VER
	SPSSOPENWRITE spssOpenWrite = (SPSSOPENWRITE)GetProcAddress(HLib, "spssOpenWrite");
	SPSSSETVARNAME spssSetVarName = (SPSSSETVARNAME)GetProcAddress(HLib, "spssSetVarName");
	SPSSSETVARLABEL spssSetVarLabel = (SPSSSETVARLABEL)GetProcAddress(HLib, "spssSetVarLabel");
	SPSSSETVARNVALUELABEL spssSetVarNValueLabel = (SPSSSETVARNVALUELABEL)GetProcAddress(HLib, "spssSetVarNValueLabel");
	SPSSCOMMITHEADER spssCommitHeader = (SPSSCOMMITHEADER)GetProcAddress(HLib, "spssCommitHeader");
	SPSSGETVARHANDLE spssGetVarHandle = (SPSSGETVARHANDLE)GetProcAddress(HLib, "spssGetVarHandle");
	SPSSSETVALUENUMERIC spssSetValueNumeric = (SPSSSETVALUENUMERIC)GetProcAddress(HLib, "spssSetValueNumeric");
	SPSSCOMMITCASERECORD spssCommitCaseRecord = (SPSSCOMMITCASERECORD)GetProcAddress(HLib, "spssCommitCaseRecord");
	SPSSCLOSEWRITE spssCloseWrite = (SPSSCLOSEWRITE)GetProcAddress(HLib, "spssCloseWrite");
	SPSSSYSMISVAL spssSysmisVal = (SPSSSYSMISVAL)GetProcAddress(HLib, "spssSysmisVal");
	SPSSSETIDSTRING spssSetIdString = (SPSSSETIDSTRING)GetProcAddress(HLib, "spssSetIdString");
	SPSSSETVARWRITEFORMAT spssSetVarWriteFormat = (SPSSSETVARWRITEFORMAT)GetProcAddress(HLib, "spssSetVarWriteFormat");
	SPSSSETVARPRINTFORMAT spssSetVarPrintFormat = (SPSSSETVARPRINTFORMAT)GetProcAddress(HLib, "spssSetVarPrintFormat");
	SPSSSETVALUECHAR spssSetValueChar = (SPSSSETVALUECHAR)GetProcAddress(HLib, "spssSetValueChar");
#else
	SPSSOPENWRITE spssOpenWrite = (SPSSOPENWRITE)dlsym(HLib, "spssOpenWrite");
	SPSSSETVARNAME spssSetVarName = (SPSSSETVARNAME)dlsym(HLib, "spssSetVarName");
	SPSSSETVARLABEL spssSetVarLabel = (SPSSSETVARLABEL)dlsym(HLib, "spssSetVarLabel");
	SPSSSETVARNVALUELABEL spssSetVarNValueLabel = (SPSSSETVARNVALUELABEL)dlsym(HLib, "spssSetVarNValueLabel");
	SPSSCOMMITHEADER spssCommitHeader = (SPSSCOMMITHEADER)dlsym(HLib, "spssCommitHeader");
	SPSSGETVARHANDLE spssGetVarHandle = (SPSSGETVARHANDLE)dlsym(HLib, "spssGetVarHandle");
	SPSSSETVALUENUMERIC spssSetValueNumeric = (SPSSSETVALUENUMERIC)dlsym(HLib, "spssSetValueNumeric");
	SPSSCOMMITCASERECORD spssCommitCaseRecord = (SPSSCOMMITCASERECORD)dlsym(HLib, "spssCommitCaseRecord");
	SPSSCLOSEWRITE spssCloseWrite = (SPSSCLOSEWRITE)dlsym(HLib, "spssCloseWrite");
	SPSSSYSMISVAL spssSysmisVal = (SPSSSYSMISVAL)dlsym(HLib, "spssSysmisVal");
	SPSSSETIDSTRING spssSetIdString = (SPSSSETIDSTRING)dlsym(HLib, "spssSetIdString");
	SPSSSETVARWRITEFORMAT spssSetVarWriteFormat = (SPSSSETVARWRITEFORMAT)dlsym(HLib, "spssSetVarWriteFormat");
	SPSSSETVARPRINTFORMAT spssSetVarPrintFormat = (SPSSSETVARPRINTFORMAT)dlsym(HLib, "spssSetVarPrintFormat");
	SPSSSETVALUECHAR spssSetValueChar = (SPSSSETVALUECHAR)dlsym(HLib, "spssSetValueChar");
	char *err = dlerror();
	if (err != nullptr) {
		SF_error(err);
		return(198);
	}
#endif

	if (!spssOpenWrite
		|| !spssSetVarName
		|| !spssSetVarLabel
		|| !spssSetVarNValueLabel
		|| !spssCommitHeader
		|| !spssGetVarHandle
		|| !spssSetValueNumeric
		|| !spssCommitCaseRecord
		|| !spssCloseWrite
		|| !spssSysmisVal
		|| !spssSetIdString
		|| !spssSetVarWriteFormat
		|| !spssSetVarPrintFormat
		|| !spssSetValueChar
		) {
		SF_error("Missing function\n");
		return(198);
	}

	bool trunc = false;
	bool validnames = true;

	ST_retcode rc;
	char buf[1000];

	if (rc = SF_macro_use("_nvars", buf, sizeof(buf))) return(rc);
	double nvars = 0;
	if (rc = SF_scal_use(buf, &nvars)) return(rc);

	if (rc = SF_macro_use("_nobs", buf, sizeof(buf))) return(rc);
	double nobs = 0;
	if (rc = SF_scal_use(buf, &nobs)) return(rc);

	std::vector<int> vartypes(nvars);
	std::vector<std::string> varnames(nvars);

	int handle;

	int errCode = spssOpenWrite(filename, &handle);
	if (errCode != SPSS_OK) {
		SF_error("Error creating file\n");
		return(errCode);
	}

	char datalabel[65];
	if (rc = SF_macro_use("_datalabel", datalabel, 65)) return(rc);
	errCode = spssSetIdString(handle, datalabel);
	if (errCode != SPSS_OK) {
		SF_error("Error writing data label\n");
		return(errCode);
	}

	for (int i = 0; i < nvars; i++) {
		char macname[100];
		double temp = 0;

		sprintf(macname, "_var%dname", i);
		char varname[SPSS_MAX_VARNAME + 1];
		if (rc = SF_macro_use(macname, varname, SPSS_MAX_VARNAME)) return(rc);
		varnames[i] = varname;
		sprintf(macname, "_var%dtype", i);
		if (rc = SF_macro_use(macname, buf, sizeof(buf))) return(rc);
		if (rc = SF_scal_use(buf, &temp)) return(rc);
		vartypes[i] = int(temp);

		if (vartypes[i] == 0) {
			errCode = spssSetVarName(handle, varnames[i].c_str(), SPSS_NUMERIC);
			//if (errCode != SPSS_OK) { 
			//	SF_error("Error creating numeric variable\n");
			//	return(errCode);
			//}
		} else {
			errCode = spssSetVarName(handle, varnames[i].c_str(), SPSS_STRING(vartypes[i]));
			//if (errCode != SPSS_OK) { 
			//	SF_error("Error creating string variable\n");
			//	return(errCode);
			//}
		}

		if (errCode == SPSS_INVALID_VARNAME || errCode == SPSS_DUP_VAR) {
			validnames = false;
			char colname[10];
			sprintf(colname, "var%d", i + 1);
			varnames[i] = colname;
			if (vartypes[i] == 0) {
				errCode = spssSetVarName(handle, varnames[i].c_str(), SPSS_NUMERIC);
				if (errCode != SPSS_OK) {
					SF_error("Error creating numeric variable\n");
					return(errCode);
				}
			} else {
				errCode = spssSetVarName(handle, varnames[i].c_str(), SPSS_STRING(vartypes[i]));
				if (errCode != SPSS_OK) {
					SF_error("Error creating string variable\n");
					return(errCode);
				}
			}
		}

		if (varnames[i].size() > SPSS_MAX_VARNAME) //Limited to 64 characters
			trunc = true;

		sprintf(macname, "_var%ddisptype", i);
		if (rc = SF_macro_use(macname, buf, sizeof(buf))) return(rc);
		if (rc = SF_scal_use(buf, &temp)) return(rc);
		int type = int(temp);

		sprintf(macname, "_var%ddispdec", i);
		if (rc = SF_macro_use(macname, buf, sizeof(buf))) return(rc);
		if (rc = SF_scal_use(buf, &temp)) return(rc);
		int dec = int(temp);

		sprintf(macname, "_var%ddispwid", i);
		if (rc = SF_macro_use(macname, buf, sizeof(buf))) return(rc);
		if (rc = SF_scal_use(buf, &temp)) return(rc);
		int wid = int(temp);

		errCode = spssSetVarWriteFormat(handle, varnames[i].c_str(), type, dec, wid);
		if (errCode != SPSS_OK) {
			SF_error("Error writing variable format information\n");
			return(errCode);
		}
		errCode = spssSetVarPrintFormat(handle, varnames[i].c_str(), type, dec, wid);
		if (errCode != SPSS_OK) {
			SF_error("Error writing variable format information\n");
			return(errCode);
		}

		sprintf(macname, "_var%dlabel", i);
		char varlabel[121];
		if (rc = SF_macro_use(macname, varlabel, 121)) return(rc);
		errCode = spssSetVarLabel(handle, varnames[i].c_str(), varlabel);
		if (errCode != SPSS_OK) {
			SF_error("Error setting variable label\n");
			return(errCode);
		}

		sprintf(macname, "_var%dnlab", i);
		if (rc = SF_macro_use(macname, buf, sizeof(buf))) return(rc);
		if (rc = SF_scal_use(buf, &temp)) return(rc);
		int numlabs = int(temp);

		for (int j = 0; j < numlabs; j++) {
			sprintf(macname, "_var%dlab%dnum", i, j);
			if (rc = SF_macro_use(macname, buf, sizeof(buf))) return(rc);
			if (rc = SF_scal_use(buf, &temp)) return(rc);
			int labnum = int(temp);

			char labval[SPSS_MAX_VALLABEL + 1];
			sprintf(macname, "_var%dlab%dval", i, j);
			if (rc = SF_macro_use(macname, labval, 10)) return(rc);

			errCode = spssSetVarNValueLabel(handle, varnames[i].c_str(), labnum, labval);
			if (errCode != SPSS_OK) {
				SF_error("Error writing value label\n");
				return(errCode);
			}
			if (strlen(labval) > SPSS_MAX_VALLABEL) // Limited to 60 characters
				trunc = true;
		}
	}

	errCode = spssCommitHeader(handle);
	if (errCode != SPSS_OK) {
		SF_error("Error writing header\n");
		return(errCode);
	}

	for (int j = 0; j < nobs; j++) {
		for (int i = 0; i < nvars; i++) {
			double varH;
			errCode = spssGetVarHandle(handle, varnames[i].c_str(), &varH);
			if (errCode != SPSS_OK) {
				SF_error("Error getting variable handle\n");
				return(errCode);
			}

			if (vartypes[i] == 0) {
				double val;
				if (rc = SF_vdata(i + 1, j + 1, &val)) return(rc);
				if (SF_is_missing(val)) {
					errCode = spssSetValueNumeric(handle, varH, spssSysmisVal());
				} else {
					errCode = spssSetValueNumeric(handle, varH, val);
				}
				if (errCode != SPSS_OK) {
					SF_error("Error writing numeric variable\n");
					return(errCode);
				}
			} else {
				char val[256];
				if (rc = SF_sdata(i + 1, j + 1, val)) return(rc);
				errCode = spssSetValueChar(handle, varH, val);
				if (errCode != SPSS_OK) {
					SF_error("Error creating string variable\n");
					return(errCode);
				}
			}
		}
		errCode = spssCommitCaseRecord(handle);
		if (errCode != SPSS_OK) {
			SF_error("Error commiting record variable\n");
			return(errCode);
		}
	}

	errCode = spssCloseWrite(handle);
	if (errCode != SPSS_OK) {
		SF_error("Error closing file");
		return(errCode);
	}

	if (trunc == true) {
		SF_error("Some variable names/labels have been truncated\n");
	}
	if (validnames == false) {
		SF_error("Some variables names are invalid in SPSS, these have been renamed\n");
	}
	return(0);
}

STDLL stata_call(int argc, char *argv[])
{
	if (argc < 2) {
		SF_error("Invalid arguments\n");
	}

	// Load library
#ifdef _MSC_VER
	wchar_t strExePath[MAX_PATH];
	GetModuleFileName(GetModuleHandle(L"spssio.plugin"), strExePath, MAX_PATH);
	wchar_t drive[_MAX_DRIVE];
	wchar_t dir[_MAX_DIR];
	wchar_t fname[_MAX_FNAME];
	wchar_t ext[_MAX_EXT];

	_wsplitpath_s(strExePath, drive, dir, fname, ext);

	static wchar_t libpath[MAX_PATH + 1];
	swprintf(libpath, MAX_PATH, L"%s%s", drive, dir);

	SetDllDirectory(libpath);

#ifdef _WIN64
	HINSTANCE HLib = LoadLibrary(L"spssio64");
#else
	HINSTANCE HLib = LoadLibrary(L"spssio32");
#endif
	SetDllDirectory(nullptr);
#else
	Dl_info dl_info;
	dladdr((void*)stata_call, &dl_info);
	std::string path = dl_info.dli_fname;
	auto pos = path.rfind("/");
	if (pos != std::string::npos) {
		path = path.substr(0, pos);
	} else {
		SF_error("error finding library path\n");
	}

	// Load dependent libraries directly so that they can be found.
	// Carry on if they don't load as another version may be found later.
	// NOTE: These versions correspond with those distibuted with version 25
	// of the SPSS I/O module. Update if using a different version
#ifndef __APPLE__
	std::string libz = path + "/libzlib1211spss.so";
	std::string libgsk = path + "/libgsk8iccs_64.so";
	std::string libicudata = path + "/libicudata.so.51.2";
	std::string libicu = path + "/libicuuc.so.51.2";
	std::string libicuuc = path + "/libicuuc.so.51.2";
	std::string libicui18n = path + "/libicui18n.so.51.2";

	std::string libspss = path + "/libspssdio.so.1";
#else
	std::string libz = path + "/libzlib1211spss.dylib";
	std::string libgsk = path + "/libgsk8iccs_64.dylib";
	std::string libicudata = path + "/libicudata.51.2.dylib";
	std::string libicu = path + "/libicuuc.51.2.dylib";
	std::string libicuuc = path + "/libicuuc.51.2.dylib";
	std::string libicui18n = path + "/libicui18n.51.2.dylib";

	std::string libspss = path + "/libspssdio.dylib";
#endif
	void *HLib1 = dlopen(libz.c_str(), RTLD_LAZY|RTLD_GLOBAL);
	void *HLib2 = dlopen(libgsk.c_str(), RTLD_LAZY|RTLD_GLOBAL);
	void *HLib3 = dlopen(libicudata.c_str(), RTLD_LAZY|RTLD_GLOBAL);
	void *HLib4 = dlopen(libicu.c_str(), RTLD_LAZY|RTLD_GLOBAL);
	void *HLib5 = dlopen(libicuuc.c_str(), RTLD_LAZY|RTLD_GLOBAL);
	void *HLib6 = dlopen(libicui18n.c_str(), RTLD_LAZY|RTLD_GLOBAL);

	void *HLib = dlopen(libspss.c_str(), RTLD_LAZY);
#endif
	
	char *filename = argv[0];
	ST_retcode rc;

	if (strcmp(argv[1], "dimensions") == 0) {
		rc = getdimensions(HLib, filename);
	}

	if (strcmp(argv[1], "numlabels") == 0) {
		rc = getnumlabels(HLib, filename);
	}

	if (strcmp(argv[1], "query") == 0) {
		rc = query(HLib, filename);
	}

	if (strcmp(argv[1], "load") == 0) {
		rc = load(HLib, filename);
	}

	if (strcmp(argv[1], "save") == 0) {
		rc = save(HLib, filename);
	}

	//SF_display("Done\n") ;

#ifdef _MSC_VER
	FreeLibrary(HLib);
#else
	dlclose(HLib);

	// Close any dependent libraries that may have been loaded
	if (HLib1) {
		dlclose(HLib1);
	}
	if (HLib2) {
		dlclose(HLib2);
	}
	if (HLib3) {
		dlclose(HLib3);
	}
	if (HLib4) {
		dlclose(HLib4);
	}
	if (HLib5) {
		dlclose(HLib5);
	}
	if (HLib6) {
		dlclose(HLib6);
	}
#endif

	return(0) ;
}

