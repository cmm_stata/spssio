{smcl}
{* *! version 1  04sep2012}{...}
{vieweralsosee "spss" "help spss"}{...}
{viewerjumpto "Syntax" "spss##syntax"}{...}
{viewerjumpto "Description" "spss##description"}{...}
{viewerjumpto "Options for spss" "spss##options_savewsz"}{...}
{viewerjumpto "Examples" "spss##examples"}{...}
{viewerjumpto "Authors" "spss##authors"}{...}
{title:Title}

{p2colset 5 16 18 2}{...}
{p2col :{cmd:spss}} - Save and manipulate SPSS datasets{p_end}
{p2colreset}{...}


{marker syntax}{...}
{title:Syntax}

{p 8 13 2}

{phang}
Save and use data from disk

{p 8 13 2}
{opt spss} {cmd:fetch}

{p 8 13 2}
{opt spss} {cmd:save} [{it:{help filename}}] [{cmd:,} {it:{help spss##save_options:save_options}}]

{p 8 13 2}
{opt spss} {cmd:use}  {it:{help filename}} [{cmd:,} {it:{help spss##use_options:use_options}}]

{p 8 13 2}
{opt spss} {cmd:use}  [{varlist}] {ifin} {cmd:using} {it:{help filename}} [{cmd:,} {it:{help spss##use_options:use_options}}]

{p 8 13 2}
{opt spss} {opt describe} {cmd:using} {it:{help filename}} [{cmd:,} {it:{help spss##describe_options:describe_options}}]


{synoptset 17}{...}
{marker save_options}{...}
{synopthdr :save_options}
{synoptline}
{synopt :{opt nol:abel}}omit value labels from the saved dataset{p_end}
{synopt :{opt replace}}overwrite existing dataset{p_end}
{synopt :{opt empty:ok}}save dataset even if zero observations and zero variables{p_end}
{synoptline}
{p2colreset}{...}

{synoptset 17}{...}
{marker use_options}{...}
{synopthdr :use_options}
{synoptline}
{synopt :{opt clear}}specifies that it is okay to replace the data in memory, even though the current data have not been saved to disk.{p_end}
{synopt :{opt nol:abel}}prevents value labels in the saved data from being loaded.{p_end}
{synoptline}
{p2colreset}{...}

{synoptset 17}{...}
{marker describe_options}{...}
{synopthdr :describe_options}
{synoptline}
{synopt :{opt si:mple}}display only variable names{p_end}
{synopt :{opt s:hort}}display only general information{p_end}
{synopt :{opt varl:ist}}save r(varlist) and r(sortlist) in addition to usual saved results; programmer's option{p_end}
{synoptline}
{p2colreset}{...}

{marker description}{...}
{title:Description}

{pstd}
{opt spss fetch} downloads and installs the SPSS I/O modules required for the command to function.

{pstd}
{opt spss save} stores the dataset currently in memory as an SPSS datafile under the name
{it:{help filename}}.  If {it:filename} is not specified, the name under which
the data were last known to Stata ({cmd:c(filename)}) is used.  If
{it:filename} is specified without an extension, {cmd:.sav} is used.  If your
{it:filename} contains embedded spaces, remember to enclose it in double
quotes.

{pstd}
{opt spss use} loads the dateset stored under the name {it:{help filename}} into memory.  If
{it:filename} is specified without an extension, {cmd:.sav} is used.  If your
{it:filename} contains embedded spaces, remember to enclose it in double
quotes.

{pstd}
{opt spss use} describes the dateset stored under the name {it:{help filename}}.  If
{it:filename} is specified without an extension, {cmd:.sav} is used.  If your
{it:filename} contains embedded spaces, remember to enclose it in double
quotes.

{marker examples}{...}
{title:Examples}

    Setup
{phang2}{bf:{stata "sysuse auto, clear":. sysuse auto, clear}}{p_end}

{pstd}Save data in memory to SPSS datafile{p_end}
{phang2}{bf:{stata "spss save myauto":. spss save myauto}}{p_end}

{pstd}Resave {cmd:myauto} after changing the data{p_end}
{phang2}{bf:{stata "generate price1000 = price/1000":. generate price1000 = price/1000}}{p_end}
{phang2}{bf:{stata "spss save myauto, replace":. spss save  myauto, replace}}{p_end}

{pstd}Equivalent to above command{p_end}
{phang2}{bf:{stata "spss save, replace":. spss save, replace}}

{pstd}Describe data stored in myauto{p_end}
{phang2}{bf:{stata "spss describe myauto":. spss describe myauto}}{p_end}

{pstd}Use saved data, clearing the data in memory{p_end}
{phang2}{bf:{stata "spss use myauto, clear":. spss use myauto, clear}}{p_end}


{marker authors}{...}
{title:Authors}

{p 4}Chris Charlton{p_end}
{p 4}Centre for Multilevel Modelling{p_end}
{p 4}University of Bristol{p_end}
{p 4}{browse "mailto:c.charlton@bristol.ac.uk":c.charlton@bristol.ac.uk}{p_end}

{p 4}George Leckie{p_end}
{p 4}Centre for Multilevel Modelling{p_end}
{p 4}University of Bristol{p_end}
{p 4}{browse "mailto:g.leckie@bristol.ac.uk":g.leckie@bristol.ac.uk}{p_end}
