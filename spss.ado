program spss,
	version 12.1
	
	syntax anything(everything), *

	if "`anything'" == "fetch" {
		fetchspss
		exit
	}
	
	local space = strpos(`"`anything'"', " ")

	if `space' == 0 {
		display as error "Invalid syntax"
		exit 198
	}
	
	local command = substr(`"`anything'"', 1, `=`space'-1')
	local anything = substr(`"`anything'"', `=`space'+1', .)

	if "`command'" == "use" {
		if strpos(`"`anything'"', "using") == 0 {
			readspss using `anything', `options'
		}
		else {
			readspss `anything', `options'
		}
	}
	else if "`command'" == "save" {
		writespss using `anything', `options'
	}
	else if "`command'" == "describe" {
		if strpos(`"`anything'"', "using") == 0 {
			describespss using `anything', `options'
		}
		else {
			describespss `anything', `options'
		}		
	}
	else {
		display as error "invalid command"
		exit 198
	}

end

program fetchspss
	version 12.1

	* Local plugin path
	quietly findfile spssio.plugin
	local plugpath = substr("`r(fn)'", 1, strrpos("`r(fn)'", "/"))

	if "`plugpath'" == "" {
		display as error "Plugin not found"
		return 198
	}

	tempfile temp
	mkdir "`temp'"

	* Download SPSS I/O module from url provided in:
	* https://www.ibm.com/developerworks/community/wikis/home?lang=en#!/wiki/We70df3195ec8_4f95_9773_42e448fa9029/page/Downloads+for+IBM%C2%AE+SPSS%C2%AE+Statistics
	display as text "Downloading modules... " _n
	copy https://dal05.objectstorage.softlayer.net/v1/AUTH_14da1a30-d001-4a2c-9060-cadaf68ff44d/yp-ngp-spss-dal05/SPSSTools/Statistics25/IO_Module_for_SPSS_Statistics_25.zip "`temp'/"
	capture confirm file "`temp'/IO_Module_for_SPSS_Statistics_25.zip"
	if c(rc) {
		display as error "SPSS I/O modules not found, please install manually"
		return 198
	}
	else {
		local dir : pwd
		quietly cd "`temp'"
		* Unzip and copy library
		unzipfile "`temp'/IO_Module_for_SPSS_Statistics_25.zip"
		quietly cd "`dir'"
		type "`temp'/license/LA_en"
		local ans
		while "`ans'" ~= "y" & "`ans'" ~= "n" {
			display "Do you accept the above license y/n?" _request(_ans)
		}
		if "`ans'" == "n" {
			exit
		}
		
		if c(os) == "Windows" {
			if c(bit) == 64 {
				// Copy win64 directory contents
				copy "`temp'/win64/gsk8iccs_64.dll" "`plugpath'"
				copy "`temp'/win64/icudt51.dll" "`plugpath'"
				copy "`temp'/win64/icuin51.dll" "`plugpath'"
				copy "`temp'/win64/icuuc51.dll" "`plugpath'"
				copy "`temp'/win64/spssio64.dll" "`plugpath'"
				copy "`temp'/win64/zlib1211spss.dll" "`plugpath'"
			}
			else {
				// Copy win32 directory contents
				copy "`temp'/win32/gsk8iccs.dll" "`plugpath'"
				copy "`temp'/win32/icudt51.dll" "`plugpath'"
				copy "`temp'/win32/icuin51.dll" "`plugpath'"
				copy "`temp'/win32/icuuc51.dll" "`plugpath'"
				copy "`temp'/win32/spssio32.dll" "`plugpath'"
				copy "`temp'/win32/zlib1211spss.dll" "`plugpath'"
			}
		}
		if c(os) == "MacOSX" {
			// Copy macos directory contents
			copy "`temp'/macos/libgsk8iccs.dylib" "`plugpath'"
			copy "`temp'/macos/libicudata.51.2.dylib" "`plugpath'"
			copy "`temp'/macos/libicui18n.51.2.dylib" "`plugpath'"
			copy "`temp'/macos/libicuuc.51.2.dylib" "`plugpath'"
			copy "`temp'/macos/libspssdio.dylib" "`plugpath'"
			copy "`temp'/macos/libzlib1211spss.dylib" "`plugpath'"
		}
		if c(os) == "Unix" {
			// Copy lin64 directory contents
			copy "`temp'/lin64/libgsk8iccs_64.so" "`plugpath'"
			copy "`temp'/lin64/libicudata.so.51.2" "`plugpath'"
			copy "`temp'/lin64/libicui18n.so.51.2" "`plugpath'"
			copy "`temp'/lin64/libicuuc.so.51.2" "`plugpath'"
			copy "`temp'/lin64/libspssio.so.1" "`plugpath'"
			copy "`temp'/lin64/libzlib1211spss.so" "`plugpath'"
		}
		display as text "Done" _n
	}
end

program readspss
	version 12.1
	//syntax [namelist] [using/] [, CLEAR NOLabel IN(string) IF(string)]
	syntax [anything(everything equalok)] [, CLEAR NOLabel]
	
	local part namelist
	forvalues i = 1/`:word count `anything'' {
		local current :word `i' of `anything'
		if "`current'" == "if" {
			local part if
		}
		else if  "`current'" == "in" {
			local part in
		}
		else if "`current'" == "using" {
			local part using
		} 
		else {
			local `part' ``part'' `current'
		}
	}
		
	if `c(changed)' == 1 & "`clear'" == "" {
		display as error "no; data in memory would be lost"
		exit 4
	}
	
	if "`using'" == "" {
		// use current file name and strip off .dta
		local filename `=reverse(substr(reverse("`c(filename)'"), 5, .))'
	}
	
	if lower(substr("`using'", -4,.)) ~= ".sav" {
		local using `using'.sav
	}

	// Fetch dimensions of data
	tempname nobs
	scalar `nobs' = 0
	tempname nvars
	scalar `nvars' = 0
	plugin call spssio, "`using'" "dimensions"

	// Fetch other information from file
	forvalues i = 0/`=`nvars'-1' {
		tempname var`i'type
		scalar `var`i'type' = 0
		tempname var`i'disptype
		scalar `var`i'disptype' = 0
		tempname var`i'dispdec
		scalar `var`i'dispdec' = 0
		tempname var`i'dispwid
		scalar `var`i'dispwid' = 0
		tempname var`i'nlab
		scalar `var`i'nlab' = 0
	}

	plugin call spssio, "`using'" "numlabels"
	forvalues i = 0/`=`nvars'-1' {
		forvalues j = 0/`=`var`i'nlab'-1' {
			tempname var`i'lab`j'num
			scalar `var`i'lab`j'num' = 0
		}
	}

	plugin call spssio, "`using'" "query"
	
	if "`namelist'" ~= "" {
		local dupnames :list dups namelist
		if "`dupnames'" ~= "" {
			display as error "duplicate variable names specified: `dupnames'"
			exit 198
		}
	}
	else {
		forvalues i = 0/`=`nvars'-1' {
			local namelist `namelist' `var`i'name'
		}
	}
	
	if `:list sizeof namelist' > `nvars' {
		display as error "too many variables specified"
		exit 103
	}
	
	tempname instart
	tempname inend
	if "`in'" ~= "" {
		local slash = strpos("`in'", "/")
		if `slash' == 0 {
			capture scalar `instart' = `in'
			if c(rc) == 111 {
				display as error "'`in'' invalid obs no"
				exit 198
			}
			scalar `inend' = `in'
		}
		else {
			tokenize `in', parse("/")
			if "`1'" == "f" local 1 = 1
			if "`3'" == "l" local 3 = `nobs'

			capture scalar `instart' = `1'
			if c(rc) == 111 {
				display as error "'`=substr("`in'", 1, `=`slash'-1')'' invalid obs no"
				exit 198
			}

			capture scalar `inend' = `3'
			if c(rc) == 111 {
				display as error "'`=substr("`in'", `=`slash'+1', .)'' invalid obs no"
				exit 198
			}			
		}
		if `instart' > `nobs' | `instart' < 1 | `inend' > `nobs' | `inend' < 1 | `inend' < `instart' {
			display as error "Obs. nos. out of range"
			exit 198
		}
	}
	else {
		scalar `instart' = 1
		scalar `inend' = `nobs'
	}	
	
	drop _all
	label drop _all
	label data "`dataid'"
	quietly set obs `=`inend' - `instart' + 1' //`nobs'
	forvalues i = 0/`=`nvars'-1' {
		tempname var`i'inc 
		if `:list var`i'name in namelist' {
			scalar `var`i'inc' = 1
			local namelist : list namelist - var`i'name
		}
		else {
			scalar `var`i'inc' = 0
			continue
		}
	
		if `var`i'type' == 0 {
			quietly gen double `var`i'name' = .
			local dispfmt "%`=`var`i'dispwid''.`=`var`i'dispdec''g"
			format `var`i'name' `dispfmt'
		} 
		else {
			if `var`i'type' > 244 {
				display as error "Warning: String variable `var`i'name' has more than 244 characters, this will be truncated"
				scalar `var`i'type' = 244
			}
			quietly gen str`=`var`i'type'' `var`i'name' = ""		
		}
		label variable `var`i'name' "`var`i'label'"
		
		if `var`i'nlab' > 0 & "`nolabel'" == ""{
			if `var`i'type' == 0 {
				forvalues j = 0/`=`var`i'nlab'-1' {
					label define `var`i'name' `=`var`i'lab`j'num'' "`var`i'lab`j'val'", add
				}
				label values `var`i'name' `var`i'name'
			}
			else {
				display as error "Warning: Dataset contains value labels for `var`i'name', which Stata does not allow"
			}
		}
	}
	
	if "`namelist'" ~= "" {
		display as error "variables `namelist' not found"
		exit 111
	}
	unab varlist : *
		
	plugin call spssio `varlist', "`using'" "load"

	forvalues i = 0/`=`nvars'-1' {
		if `var`i'inc' == 0 {
			continue
		}
		if `var`i'type' == 0 {
			if `var`i'disptype' >= 20 & `var`i'disptype' <= 25 | `var`i'disptype' >= 28 & `var`i'disptype' <= 30 | `var`i'disptype' >= 38 & `var`i'disptype' <= 39 {
				if `var`i'disptype' == 21 | `var`i'disptype' == 22 | `var`i'disptype' == 25 {
					quietly replace `var`i'name' = (`var`i'name'*1000) + tc(14oct1582 00:00)
					format `var`i'name' %tc
				}
				else if `var`i'disptype' == 28 {
					quietly replace `var`i'name' = mofd(dofc((`var`i'name'*1000) + tc(14oct1582 00:00)))
					format `var`i'name' %tm
				}
				else if `var`i'disptype' == 29 {
					quietly replace `var`i'name' = qofd(dofc((`var`i'name'*1000) + tc(14oct1582 00:00)))
					format `var`i'name' %tq
				}
				else if `var`i'disptype' == 30 {
					quietly replace `var`i'name' = wofd(dofc((`var`i'name'*1000) + tc(14oct1582 00:00)))
					format `var`i'name' %tw		
				}
				else {
					quietly replace `var`i'name' = dofc((`var`i'name'*1000) + tc(14oct1582 00:00))
					format `var`i'name' %td
				}
			}
		}
	}

	quietly compress
	if `"`if'"' != "" {
		quietly keep if `if'
	}
	if "`dataid'" ~= "" {
		display as text "(`dataid')"
		display as text ""
	}
	
end

program writespss,
	version 12.1
	syntax using/ [, REPLACE NOLabel EMPTYok]

	if lower(substr("`using'", -4,.)) ~= ".sav" {
		local using `using'.sav
	}	
	
	capture confirm new file "`using'"
	
	if c(rc) & "`replace'" == "" {
		display as error "file `using' already exists"
		exit 602
	}
	
	unab varlist : *
	tempname nobs
	scalar `nobs' = _N
	tempname nvars
	scalar `nvars' = `:list sizeof varlist'
	
	if `nobs' == 0 & `nvars' == 0 & "`emptyok'" == "" {
		display as error "no variables defined"
		exit 111
	}
	
	if `nobs' == 0 {
		display as text "(note: dataset contains 0 observations)"
	}

	local datalabel "`:data label'"
	preserve
	forvalues i = 0/`=`nvars'-1' {
		local var`i'name "`:word `=`i'+1' of `varlist''"
		capture confirm string variable `var`i'name'
		tempname var`i'type
		tempname var`i'dispwid
		tempname var`i'dispdec
		tempname var`i'disptype
		tempname var`i'nlab
		if c(rc) {
			scalar `var`i'type' = 0
			scalar `var`i'dispwid' = 9
			scalar `var`i'dispdec' = 0
			scalar `var`i'disptype' = 5
			
			* NOTE: These conversions could lose some accuracy due to leap seconds
			if "`:format `var`i'name''" == "%tc" {
				scalar `var`i'disptype' = 22
				scalar `var`i'dispdec' = 0
				scalar `var`i'dispwid' = 20
				quietly replace `var`i'name' = (`var`i'name' - tc(14oct1582 00:00)) / 1000
			}

			if "`:format `var`i'name''" == "%tC" {
				scalar `var`i'disptype' = 22
				scalar `var`i'dispdec' = 0
				scalar `var`i'dispwid' = 20
				quietly replace `var`i'name' = (`var`i'name' - tC(14oct1582 00:00)) / 1000
			}			
			
			if "`:format `var`i'name''" == "%td" {
				scalar `var`i'disptype' = 20
				scalar `var`i'dispwid' = 11
				quietly replace `var`i'name' = (cofd(`var`i'name') - tc(14oct1582 00:00)) / 1000
			}
					
			if "`:format `var`i'name''" == "%tm" {
				scalar `var`i'disptype' = 28
				scalar `var`i'dispwid' = 9
				quietly replace `var`i'name' = (cofd(dofm(`var`i'name')) - tc(14oct1582 00:00)) / 1000
			}
			
			if "`:format `var`i'name''" == "%tq" {
				scalar `var`i'disptype' = 29
				scalar `var`i'dispwid' = 8
				quietly replace `var`i'name' = (cofd(dofq(`var`i'name')) - tc(14oct1582 00:00)) / 1000
			}
			
			if "`:format `var`i'name''" == "%tw" {
				scalar `var`i'disptype' = 30
				scalar `var`i'dispwid' = 8
				quietly replace `var`i'name' = (cofd(dofw(`var`i'name')) - tc(14oct1582 00:00)) / 1000
			}
			
			* NOTE: These last two types do not have an equivalent in SPSS, so save as DateTime
			if "`:format `var`i'name''" == "%th" {
				scalar `var`i'disptype' = 20
				scalar `var`i'dispwid' = 11
				quietly replace `var`i'name' = (cofd(dofh(`var`i'name')) - tc(14oct1582 00:00)) / 1000
			}
			
			if "`:format `var`i'name''" == "%ty" {
				scalar `var`i'disptype' = 20
				scalar `var`i'dispwid' = 11
				quietly replace `var`i'name' = (cofd(dofy(`var`i'name')) - tc(14oct1582 00:00)) / 1000
			}					

			local labname "`:value label `var`i'name''"
			scalar `var`i'nlab' = 0
			if "`labname'" ~= "" & "`nolabel'" == "" {
				local varlab
				capture label list `labname'
				if "`r(k)'" ~= "" {
					forvalues j = `r(min)'/`r(max)' {
						local varlab `:label `labname' `j''
						if "`varlab'" ~= "" {
							tempname var`i'lab`j'num
							scalar `var`i'lab`j'num' = `var`i'nlab'
							local var`i'lab`j'val = "`varlab'"
							scalar `var`i'nlab' = `var`i'nlab' + 1
						}
					}
				}
			}
		}
		else {
			scalar `var`i'type' = `=subinstr("`:type `var`i'name''", "str", "",.)'
			scalar `var`i'dispwid' = `var`i'type'
			scalar `var`i'dispdec' = 0
			scalar `var`i'disptype' = 1
			scalar `var`i'nlab' = 0	
		}
		local var`i'label "`:variable label `var`i'name''"
	}
	
	plugin call spssio `varlist', "`using'" "save"
	restore
	display as text "file `using' saved"
	display as text ""
	
end

program describespss, rclass
	version 12.1
	syntax using/ , [VARList Short SImple]

	if "`simple'" ~= "" {
		if "`short'" ~= "" {
			display as error "simple may not be combined with other options"
			exit 198
		}
	}
	
	if lower(substr("`using'", -4,.)) ~= ".sav" {
		local using `using'.sav
	}
	
	// Fetch dimensions of data
	tempname nobs
	scalar `nobs' = 0
	tempname nvars
	scalar `nvars' = 0
	plugin call spssio, "`using'" "dimensions"

	// Fetch other information from file
	forvalues i = 0/`=`nvars'-1' {
		tempname var`i'type
		scalar `var`i'type' = 0
		tempname var`i'disptype
		scalar `var`i'disptype' = 0
		tempname var`i'dispdec
		scalar `var`i'dispdec' = 0
		tempname var`i'dispwid
		scalar `var`i'dispwid' = 0
		tempname var`i'nlab
		scalar `var`i'nlab' = 0
	}

	plugin call spssio, "`using'" "numlabels"
	forvalues i = 0/`=`nvars'-1' {
		forvalues j = 0/`=`var`i'nlab'-1' {
			tempname var`i'lab`j'num
			scalar `var`i'lab`j'num' = 0
		}
	}
	
	plugin call spssio, "`using'" "query"
	
	if "`simple'" ~= "" {
		local maxlen = 0
		forvalues i = 0/`=`nvars'-1' {
			if length("`var`i'name'") > `maxlen' {
				local maxlen = length("`var`i'name'")
			}
		}
		local totalwidth = 1
		forvalues i = 0/`=`nvars'-1' {
			display as text _col(`totalwidth') "`var`i'name'" _continue
			if `totalwidth' + `maxlen' > 80 {
				local totalwidth = 1
				display as text ""
			}
			else {
				local totalwidth = `totalwidth' + `maxlen' + 2
			}
		}
		display as text ""
	}
	else {
		display as text "Contains data"
		local timestamp = clock("`date' `time'", "DMYhms", 2067)
		display as text _col(3) "obs:" as result _col(12) %9.0gc `nobs' as result _col(47) %tcDD_Mon_CCYY_HH:MM `timestamp'
		display as text _col(2) "vars:" as result _col(12) %9.0gc `nvars'
		display as text _col(2) "size:" as result _col(12) %9.0gc `=`nobs'*`nvars'*8'
		display as text "{hline 80}"
		display as text                        _col(15) "storage" as text _col(24) "display" as text _col(36) "value"
		display as text _col(1) "variable name" as text _col(17) "type" as text _col(24) "format" as text _col(36) "label" as text _col(47) "variable label"
		if "`short'" == "" {
			display as text "{hline 80}"
			forvalues i = 0/`=`nvars'-1' {
				if `var`i'nlab' > 0 {
					local varlab "`var`i'name'"
				}
				else {
					local varlab ""
				}
				if `var`i'type' == 0 {
					if `var`i'disptype' >= 20 & `var`i'disptype' <= 25 | `var`i'disptype' >= 28 & `var`i'disptype' <= 30 | `var`i'disptype' >= 38 & `var`i'disptype' <= 39 {
						if `var`i'disptype' == 21 | `var`i'disptype' == 22 | `var`i'disptype' == 25 {
							local dispfmt "%tc"
						}
						else if `var`i'disptype' == 28 {
							local dispfmt "%tm"
						}
						else if `var`i'disptype' == 29 {
							local dispfmt "%tq"
						}
						else if `var`i'disptype' == 30 {
							local dispfmt "%tw"				
						}
						else {
							local dispfmt "%td"
						}
					}
					else {
						local dispfmt "%`=`var`i'dispwid''.`=`var`i'dispdec''g"
					}
					if length("`varlab'") < 9 {
						display as result _col(1) "`var`i'name'" as text _col(16) "double" as text _col(24) "`dispfmt'" as text _col(36) "`varlab'" as result _col(47) "`var`i'label'"
					}
					else {
						display as result _col(1) "`var`i'name'" as text _col(16) "double" as text _col(24) "`dispfmt'" as text _col(36) "`varlab'"
						display as result _col(47) "`var`i'label'"
					}
				}
				else {
					local size = 20 - length("`var`i'type'")
					display as result _col(1) "`var`i'name'" as text _col(16) "s`=`var`i'type''" as text _col(24) "%9s" as result _col(47) "`var`i'label'"
				}
			}
			display as text "{hline 80}"
		}
		display as text "Sorted by:"
	}
	return clear
	return scalar k = `nvars'
	return scalar N = `nobs'
	return scalar width = `nvars' * 8
	if "`varlist'" ~= "" {
		local varnames
		forvalues i = 0/`=`nvars'-1' {
			local varnames `varnames' `var`i'name'
		}
		return local varlist = "`varnames'"
		return local sortlist = ""
	}
	
end
program spssio, plugin
